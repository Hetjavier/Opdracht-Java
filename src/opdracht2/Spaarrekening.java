/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package opdracht2;

import java.util.Objects;

/**
 *
 * @author Timpa
 */
public class Spaarrekening extends Rekening {

    protected int bedrag;
    
    public Spaarrekening() {
    }

    public Spaarrekening(Rekeningnummer rekeningnummer, int bedrag) {
        this.rekeningnummer = rekeningnummer;
        this.bedrag = bedrag;
    }

    @Override
    public int getBedrag() {
        return bedrag;
    }

    public void setBedrag(int bedrag) {
        this.bedrag = bedrag;
    }

    @Override
    public void addVerrichting(Verrichting verrichting) {
        this.verrichting.add(verrichting);
        this.setBedrag(getBedrag() + verrichting.getBedrag());
    }
    

    @Override
    public String toString() {
        return "SPAARREKENING " + this.rekeningnummer + " BEDRAG = " + this.bedrag;
    }


    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Spaarrekening other = (Spaarrekening) obj;
        if (!Objects.equals(this.bedrag, other.bedrag)) {
            return false;
        }
        if (!Objects.equals(this.verrichting, other.verrichting)) {
            return false;
        }
        return true;
    }
    
}
