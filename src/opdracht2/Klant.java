/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package opdracht2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

/**
 *
 * @author Timpa
 */
public class Klant {

    protected String klantID;
    protected String voornaam;
    protected String familienaam;
    protected Adres adres;
    protected String paswoord;
    protected ArrayList<Rekening> rekeningen = new ArrayList<>();
    
    public Klant() {
    }

    public Klant(String klantID, String voornaam, String familienaam, Adres adres, String paswoord) {
        this.klantID = klantID;
        this.voornaam = voornaam;
        this.familienaam = familienaam;
        this.adres = adres;
        this.paswoord = paswoord;
    }

    public String getKlantID() {
        return klantID;
    }

    public void setKlantID(String klantID) {
        this.klantID = klantID;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public String getFamilienaam() {
        return familienaam;
    }

    public void setFamilienaam(String familienaam) {
        this.familienaam = familienaam;
    }

    public Adres getAdres() {
        return adres;
    }

    public void setAdres(Adres adres) {
        this.adres = adres;
    }

    public String getPaswoord() {
        return paswoord;
    }

    public void setPaswoord(String paswoord) {
        this.paswoord = paswoord;
    }

    public List<Rekening> getRekeningen() {
        return rekeningen;
    }

    public void setRekeningen(ArrayList<Rekening> rekeningen) {
        this.rekeningen = rekeningen;
    }

    public void addRekening(Rekening rekening) {
        this.rekeningen.add(rekening);
    }

    @Override
    public String toString() {
        return "KLANTID = " + klantID + "\n" + "VOORNAAM = " + voornaam + "\n" + "FAMILIENAAM = " + familienaam + "\n" + this.adres + "";
    }

}
