/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package opdracht2;

import java.util.GregorianCalendar;
import java.util.Objects;

/**
 *
 * @author Timpa
 */
public class Verrichting {

    GregorianCalendar datum;
    protected int bedrag;
    protected String omschrijving;
    protected Rekeningnummer rekeningAfkomstig;
    
    public Verrichting() { 
    }

    public Verrichting(GregorianCalendar datum, int bedrag, String omschrijving, Rekeningnummer rekeningAfkomstig) {
        this.datum = datum;
        this.bedrag = bedrag;
        this.omschrijving = omschrijving;
        this.rekeningAfkomstig = rekeningAfkomstig;
    }

    public Verrichting(GregorianCalendar datum, int bedrag, String omschrijving) {
        this.datum = datum;
        this.bedrag = bedrag;
        this.omschrijving = omschrijving;
    }

    public GregorianCalendar getDatum() {
        return datum;
    }

    public void setDatum(GregorianCalendar datum) {
        this.datum = datum;
    }

    public int getBedrag() {
        return bedrag;
    }

    public void setBedrag(int bedrag) {
        this.bedrag = bedrag;
    }

    public String getOmschrijving() {
        return omschrijving;
    }

    public void setOmschrijving(String omschrijving) {
        this.omschrijving = omschrijving;
    }

    public Rekeningnummer getRekeningAfkomstig() {
        return rekeningAfkomstig;
    }

    public void setRekeningAfkomstig(Rekeningnummer rekeningAfkomstig) {
        this.rekeningAfkomstig = rekeningAfkomstig;
    }

    @Override
    public String toString() {
         return datum.get(datum.DAY_OF_MONTH) + "/" + (datum.get(datum.MONTH) + 1 ) + "/" + datum.get(datum.YEAR) +  "\t\t\t " + bedrag + "\n" + omschrijving + "\n" + rekeningAfkomstig + "\n";
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Verrichting other = (Verrichting) obj;
        if (Double.doubleToLongBits(this.bedrag) != Double.doubleToLongBits(other.bedrag)) {
            return false;
        }
        if (!Objects.equals(this.omschrijving, other.omschrijving)) {
            return false;
        }
        if (!Objects.equals(this.datum, other.datum)) {
            return false;
        }
        if (!Objects.equals(this.rekeningAfkomstig, other.rekeningAfkomstig)) {
            return false;
        }
        return true;
    }

}
