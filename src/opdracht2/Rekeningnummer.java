/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package opdracht2;

/**
 *
 * @author Timpa
 */
public class Rekeningnummer {

    protected String groep1, groep2, groep3, groep4;
    protected long rekeningnummer;
    
    public Rekeningnummer() {     
    }

    public Rekeningnummer(String groep1, String groep2, String groep3, String groep4) {
        this.groep1 = groep1;
        this.groep2 = groep2;
        this.groep3 = groep3;
        this.groep4 = groep4;
    }

    public String getGroep1() {
        return groep1;
    }

    public void setGroep1(String groep1) {
        this.groep1 = groep1;
    }

    public String getGroep2() {
        return groep2;
    }

    public void setGroep2(String groep2) {
        this.groep2 = groep2;
    }

    public String getGroep3() {
        return groep3;
    }

    public void setGroep3(String groep3) {
        this.groep3 = groep3;
    }

    public String getGroep4() {
        return groep4;
    }

    public void setGroep4(String groep4) {
        this.groep4 = groep4;
    }

    public long getRekeningnummer() {
        return rekeningnummer;
    }

    public void setRekeningnummer(long rekeningnummer) {
        this.rekeningnummer = rekeningnummer;
    }

    private boolean isNumeric(String strNum) {

        if (strNum == null) {

            return false;

        }

        try {

            double d = Double.parseDouble(strNum);

        } catch (NumberFormatException nfe) {

            return false;

        }

        return true;

    }

      public boolean isCorrect() {
        String groepCijfers = groep2 + groep3 + groep4.substring(0, 2);
        String IBANCtrlG = groep1.substring(2, 4);
        String BelCtrlG = groep4.substring(2, 4);
        if (isNumeric(IBANCtrlG) && isNumeric(BelCtrlG) && isNumeric(groep2) && isNumeric(groep3) && isNumeric(groep4)) {
            long groepAsLong = Long.parseLong(groepCijfers);
            int belCtrlAsInt = Integer.parseInt(BelCtrlG);
            int ibanCtrlAsInt = Integer.parseInt(IBANCtrlG);
            if ((groepAsLong % 97 == belCtrlAsInt) || (groepAsLong % 97 == 0 && belCtrlAsInt == 97)) {
                if (ibanCtrlAsInt == (54 + belCtrlAsInt * 86) % 97) {
                    return true;
                }
            }
        }
        return false;
    }
    @Override
    public String toString() {
        return String.join(" ", groep1, groep2, groep3, groep4);
    }
}
