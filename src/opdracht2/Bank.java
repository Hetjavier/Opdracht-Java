/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package opdracht2;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Timpa
 */
public class Bank {

    protected String bankNaam;
    ArrayList<Klant> klanten = new ArrayList<>();

    public Bank() {
    }

    public Bank(String bankNaam) {
        this.bankNaam = bankNaam;
    }

    public String getBankNaam() {
        return bankNaam;
    }

    public void setBankNaam(String bankNaam) {
        this.bankNaam = bankNaam;
    }

    public ArrayList<Klant> getKlanten() {
        return klanten;
    }

    public void setKlanten(ArrayList<Klant> klanten) {
        this.klanten = klanten;
    }
    
    public void addKlant(Klant nieuweKlant) {
        this.klanten.add(nieuweKlant);
 }   

    @Override
    public String toString() {
        return "Bank{" + "bankNaam=" + bankNaam + ", klanten=" + klanten + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Bank other = (Bank) obj;
        if (!Objects.equals(this.bankNaam, other.bankNaam)) {
            return false;
        }
        if (!Objects.equals(this.klanten, other.klanten)) {
            return false;
        }
        return true;
    }

}
