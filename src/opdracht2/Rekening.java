/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package opdracht2;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Timpa
 */
public abstract class Rekening {
    protected Rekeningnummer rekeningnummer;
    protected ArrayList <Verrichting> verrichting = new ArrayList<>();
    
    public Rekening() {
    }

    public Rekening(Rekeningnummer rekeningnummer) {
        this.rekeningnummer = rekeningnummer;
    }

    public Rekeningnummer getRekeningnummer() {
        return rekeningnummer;
    }

    public void setRekeningnummer(Rekeningnummer rekeningnummer) {
        this.rekeningnummer = rekeningnummer;
    }

    public ArrayList<Verrichting> getVerrichtingen() {
        return verrichting;
    }

    public void setVerrichting(ArrayList<Verrichting> verrichting) {
        this.verrichting = verrichting;
    }
    
    public void addVerrichting(Verrichting verrichting) {
    this.verrichting.add(verrichting); }
    
    public abstract int getBedrag(); 

    @Override
    public String toString() {
        return "Rekening{" + "rekeningnummer=" + rekeningnummer + ", verrichting=" + verrichting + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Rekening other = (Rekening) obj;
        if (!Objects.equals(this.rekeningnummer, other.rekeningnummer)) {
            return false;
        }
        if (!Objects.equals(this.verrichting, other.verrichting)) {
            return false;
        }
        return true;
    }
    
   
}
