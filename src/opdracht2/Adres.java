/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package opdracht2;

/**
 *
 * @author Timpa
 */
public class Adres {

    protected String straat;
    protected String huisnummer;
    protected String postcode;
    protected String gemeente;

    public Adres() {
    }

    public Adres(String straat, String huisnummer, String postcode, String gemeente) {
        this.straat = straat;
        this.huisnummer = huisnummer;
        this.postcode = postcode;
        this.gemeente = gemeente;
    }

    public String getStraat() {
        return straat;
    }

    public void setStraat(String straat) {
        this.straat = straat;
    }

    public String getHuisnummer() {
        return huisnummer;
    }

    public void setHuisnummer(String huisnummer) {
        this.huisnummer = huisnummer;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getGemeente() {
        return gemeente;
    }

    public void setGemeente(String gemeente) {
        this.gemeente = gemeente;
    }

    @Override
    public String toString() {
        return "ADRES = " + straat + " " + huisnummer + "\n" + "WOONPLAATS = " + gemeente;
    }

    public boolean equals(Adres adres) {
        return this.straat.equalsIgnoreCase(adres.straat)
                && this.huisnummer.equalsIgnoreCase(adres.huisnummer)
                && this.postcode.equalsIgnoreCase(adres.postcode)
                && this.gemeente.equalsIgnoreCase(adres.gemeente);

    }
}
