/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package opdracht2;

/**
 *
 * @author Timpa
 */
public class Zichtrekening extends Rekening {
    
    protected int bedrag;
    
    public Zichtrekening() {
    }

    public Zichtrekening(Rekeningnummer rekeningnummer, int bedrag) {
        this.rekeningnummer = rekeningnummer;
        this.bedrag = bedrag;
    }

    @Override
    public int getBedrag() {
        return bedrag;
    }

    public void setBedrag(int bedrag) {
        this.bedrag = bedrag;
       
    }

    @Override
    public void addVerrichting(Verrichting verrichting) {
        this.verrichting.add(verrichting);
        this.setBedrag(getBedrag() + verrichting.getBedrag());
    }

    @Override
    public String toString() {
        return "ZICHTREKENING " + this.rekeningnummer + " BEDRAG = " + bedrag;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Zichtrekening other = (Zichtrekening) obj;
        if (Double.doubleToLongBits(this.bedrag) != Double.doubleToLongBits(other.bedrag)) {
            return false;
        }
        return true;
    }
}
